require 'sinatra/base'

module MrAlfred
  # the module for checking the status of mr alfred
  class Web < Sinatra::Base
    get '/' do
      'Mr. Alfred is alive!'
    end
  end
end
