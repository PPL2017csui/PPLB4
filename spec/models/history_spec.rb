require 'rails_helper'

RSpec.describe History, type: 'model' do
  subject { described_class.new }

  it 'is valid with valid attributes' do
    subject.user_activites = 'Anything'
    subject.user_id = 'Anything'
    subject.knowledge_base_id = 'Anything'
    expect(subject).to be_valid
  end

  it 'is not valid without a user_activites' do
    expect(subject).to_not be_valid
  end

  it 'is not valid without a user_id' do
    subject.user_activites = 'Anything'
    expect(subject).to_not be_valid
  end

  it 'is not valid without a knowledge_base_id' do
    subject.user_activites = 'Anything'
    subject.user_id = 'Anything'
    expect(subject).to_not be_valid
  end
end
