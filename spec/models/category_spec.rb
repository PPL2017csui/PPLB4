require 'rails_helper'

RSpec.describe Category, type: 'model' do
  subject { described_class.new }

  it 'is valid with valid attributes' do
    subject.category_type = 'Anything'
    expect(subject).to be_valid
  end

  it 'is not valid without a category_type' do
    expect(subject).to_not be_valid
  end
end
