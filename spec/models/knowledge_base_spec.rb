require 'rails_helper'

RSpec.describe KnowledgeBase, type: 'model' do
  subject { described_class.new }

  it 'is valid with valid attributes' do
    subject.question = 'Anything'
    subject.answer = 'Anything'
    subject.category_id = 'Anything'
    expect(subject).to be_valid
  end

  it 'is not valid without a question' do
    expect(subject).to_not be_valid
  end

  it 'is not valid without a answer' do
    subject.question = 'Anything'
    expect(subject).to_not be_valid
  end

  it 'is not valid without a category_id' do
    subject.question = 'Anything'
    subject.answer = 'Anything'
    expect(subject).to_not be_valid
  end
end
