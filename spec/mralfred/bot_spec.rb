require 'spec_helper'

describe MrAlfred::Bot do
  def app
    MrAlfred::Bot.instance
  end

  subject { app }

  it_behaves_like 'a slack ruby bot'
end
