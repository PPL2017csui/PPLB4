require 'spec_helper'

describe MrAlfred::Commands::Default do
  it 'lowercase' do
    expect(message: SlackRubyBot.config.user)
      .to respond_with_slack_message('Made by Funtrans')
  end
  it 'upcase' do
    expect(message: SlackRubyBot.config.user.upcase)
      .to respond_with_slack_message('Made by Funtrans')
  end
  it 'name:' do
    expect(message: "#{SlackRubyBot.config.user}:")
      .to respond_with_slack_message('Made by Funtrans')
  end
  it 'id' do
    expect(message: "<@#{SlackRubyBot.config.user_id}>")
      .to respond_with_slack_message('Made by Funtrans')
  end
end
