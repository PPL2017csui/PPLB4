require 'spec_helper'

describe MrAlfred::Commands::HelpCommand do
  def app
    MrAlfred::Bot.instance
  end
  it 'help' do
    message = <<MSG
*Mr.Alfred* - Bot Midtrans yang menjawab pertanyaan seputar kategori berikut.
- Operasional
- Biaya
- Midtrans_MAP
- Informasi Umum
- Integrasi
- Plugin

*Commands:*
*Hi* - Menampilkan hi
*Help* - Menampilkan informasi mengenai perintah yang dapat ditanyakan kepada bot
MSG
    expect(message: "#{SlackRubyBot.config.user} help")
      .to respond_with_slack_message(message)
  end
end
