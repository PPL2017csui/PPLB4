# Category of knowledge base
class Category < ApplicationRecord
  validates_presence_of :category_type
end
