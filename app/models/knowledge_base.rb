# Knowledge base class model
class KnowledgeBase < ApplicationRecord
  validates_presence_of :question
  validates_presence_of :answer
  validates_presence_of :category_id
end
