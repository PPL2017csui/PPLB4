# History of CRUD of knowledge base
class History < ApplicationRecord
  validates_presence_of :user_activites
  validates_presence_of :user_id
  validates_presence_of :knowledge_base_id
end
