$LOAD_PATH.unshift(File.dirname(__FILE__))

require 'dotenv'
Dotenv.load

require 'mralfred'
require 'web'

Thread.abort_on_exception = true

Thread.new do
  begin
    MrAlfred::Bot.run
  rescue StandardError => e
    STDERR.puts "ERROR: #{e}"
    STDERR.puts e.backtrace
    raise e
  end
end

run MrAlfred::Web
