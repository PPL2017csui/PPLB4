# Mr.Alfred

PPLB4
├── app
│   └── models
│       ├── application_record.rb
│       ├── category.rb
│       ├── concerns
│       ├── history.rb
│       ├── knowledge_base.rb
│       └── user.rb
├── bin
│   ├── bundle
│   ├── rails
│   ├── rake
│   ├── setup
│   └── update
├── config
│   ├── application.rb
│   ├── boot.rb
│   ├── cable.yml
│   ├── database.yml
│   ├── environment.rb
│   ├── environments
│   │   ├── development.rb
│   │   ├── production.rb
│   │   └── test.rb
│   ├── initializers
│   │   ├── application_controller_renderer.rb
│   │   ├── assets.rb
│   │   ├── backtrace_silencers.rb
│   │   ├── cookies_serializer.rb
│   │   ├── filter_parameter_logging.rb
│   │   ├── inflections.rb
│   │   ├── mime_types.rb
│   │   ├── new_framework_defaults.rb
│   │   ├── session_store.rb
│   │   └── wrap_parameters.rb
│   ├── locales
│   │   └── en.yml
│   ├── puma.rb
│   ├── routes.rb
│   └── secrets.yml
├── config.ru
├── coverage
│   ├── assets
│   │   └── 0.10.0
│   │       ├── application.css
│   │       ├── application.js
│   │       ├── colorbox
│   │       │   ├── border.png
│   │       │   ├── controls.png
│   │       │   ├── loading_background.png
│   │       │   └── loading.gif
│   │       ├── favicon_green.png
│   │       ├── favicon_red.png
│   │       ├── favicon_yellow.png
│   │       ├── loading.gif
│   │       ├── magnify.png
│   │       └── smoothness
│   │           └── images
│   │               ├── ui-bg_flat_0_aaaaaa_40x100.png
│   │               ├── ui-bg_flat_75_ffffff_40x100.png
│   │               ├── ui-bg_glass_55_fbf9ee_1x400.png
│   │               ├── ui-bg_glass_65_ffffff_1x400.png
│   │               ├── ui-bg_glass_75_dadada_1x400.png
│   │               ├── ui-bg_glass_75_e6e6e6_1x400.png
│   │               ├── ui-bg_glass_95_fef1ec_1x400.png
│   │               ├── ui-bg_highlight-soft_75_cccccc_1x100.png
│   │               ├── ui-icons_222222_256x240.png
│   │               ├── ui-icons_2e83ff_256x240.png
│   │               ├── ui-icons_454545_256x240.png
│   │               ├── ui-icons_888888_256x240.png
│   │               └── ui-icons_cd0a0a_256x240.png
│   └── index.html
├── db
│   ├── migrate
│   │   ├── 20170321113301_create_users.rb
│   │   ├── 20170321113318_create_categories.rb
│   │   ├── 20170321113319_create_knowledge_bases.rb
│   │   └── 20170321113321_create_histories.rb
│   ├── schema.rb
│   └── seeds.rb
├── Gemfile
├── Gemfile.lock
├── log
│   ├── development.log
│   └── test.log
├── mralfred
│   ├── bot.rb
│   └── commands
│       └── help.rb
├── mralfred.rb
├── Procfile
├── public
│   ├── 404.html
│   ├── 422.html
│   ├── 500.html
│   ├── apple-touch-icon.png
│   ├── apple-touch-icon-precomposed.png
│   ├── favicon.ico
│   └── robots.txt
├── Rakefile
├── README.md
├── spec
│   ├── models
│   │   ├── category_spec.rb
│   │   ├── history_spec.rb
│   │   ├── knowledge_base_spec.rb
│   │   └── user_spec.rb
│   ├── mralfred
│   │   ├── bot_spec.rb
│   │   └── commands
│   │       └── help_spec.rb
│   ├── rails_helper.rb
│   └── spec_helper.rb
└── web.rb

24 directories, 90 files
