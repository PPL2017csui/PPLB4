class CreateHistories < ActiveRecord::Migration[5.0]
  def change
    create_table :histories do |t|
      t.string :user_activites
      t.references :user, foreign_key: true
      t.references :knowledge_base, foreign_key: true

      t.timestamps
    end
  end
end
