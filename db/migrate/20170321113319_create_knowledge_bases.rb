class CreateKnowledgeBases < ActiveRecord::Migration[5.0]
  def change
    create_table :knowledge_bases do |t|
      t.text :question
      t.text :answer
      t.references :category, foreign_key: true

      t.timestamps
    end
  end
end
