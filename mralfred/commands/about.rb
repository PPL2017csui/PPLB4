module MrAlfred
  module Commands
    # mralfred/commands/about.rb
    class Default < SlackRubyBot::Commands::Base
      command 'about'
      match(/^(?<bot>[[:alnum:][:punct:]@<>]*)$/u)

      def self.call(client, data, _match)
        client.say(channel: data.channel, text: 'Made by Funtrans')
      end
    end
  end
end
