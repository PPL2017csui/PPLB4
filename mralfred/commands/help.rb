module MrAlfred
  module Commands
    # mralfred/commands/help.rb
    class HelpCommand < SlackRubyBot::Commands::Base
      help do
      end

      message = <<MSG
*Mr.Alfred* - Bot Midtrans yang menjawab pertanyaan seputar kategori berikut.
- Operasional
- Biaya
- Midtrans_MAP
- Informasi Umum
- Integrasi
- Plugin

*Commands:*
*Hi* - Menampilkan hi
*Help* - Menampilkan informasi mengenai perintah yang dapat ditanyakan kepada bot
MSG

      command 'help' do |client, data, _match|
        client.say(channel: data.channel, text: message, gif: 'help')
      end
    end
  end
end
